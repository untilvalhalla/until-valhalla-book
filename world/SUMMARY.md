# World of Until Valhalla

* [Premise](README.md)
* Characters
  * [Ganci, Charles SFC](characters/charles_ganci.md)
  * [Lunsford, Cheryl Dianne](characters/cheryl_lunsford.md)
  * [Lunsford, Raymond Daniel](characters/raymond_lunsford.md)
  * [Lunsford, Reginald "Reggie"](characters/reginald_lunsford.md)
  * [Phillips, Abigail](characters/abigail_phillips.md)
* Locations
  * [Ukiah, CA]()
    * [GC Recruiting Center](locations/ukiah_ca/gc_recruiting_center.md)
* Organizations
  * [Global Coalition](organizations/global_coalition.md)